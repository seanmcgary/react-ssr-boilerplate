import Server from './lib/server';
import config from '../config';

import reactRoutes from './routes/react';
import apiRoutes from './routes/api';

const server = new Server(config.server);

server.loadRoutes(apiRoutes);
server.loadRoutes(reactRoutes);

server.startServer();