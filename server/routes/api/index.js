import APIClient from '../../lib/apiClient';

export default (app, server) => {
	server.use('/api/*', app.pr((req) => {
		console.log(req);
		return APIClient.makeRequest(req.method, {
			path: req.baseUrl,
			query: req.query,
			data: req.body
		})
		.then((response) => response.body);
	}));
};