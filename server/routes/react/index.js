import React from 'react';
import * as url from 'url';
import * as _ from 'lodash';
import * as path from 'path';
import { matchRoutes } from 'react-router-config';
import { renderToString } from 'react-dom/server';
import * as fs from 'fs';
import { StaticRouter } from 'react-router';
import { Provider } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import { bindActionCreators } from 'redux';

import Routes, { staticRoutes } from '../../../app/routes/index';
import { buildServerStore } from '../../store/index';
import App from '../../../app/components/app';
import { HTMLResponse } from '../../lib/response';

console.log(path.resolve(`${__dirname}/index.html`));
const indexHtml = _.template(fs.readFileSync(path.resolve(`${__dirname}/index.html`)).toString());

function renderApplication(store, req) {
	const parsedUrl = url.parse(req.originalUrl);
	const location = parsedUrl.path;
	const context = {};

	return new Promise((resolve, reject) => {
		let renderedComponent;
		try {
			const components = React.createElement(Provider, { store },
				React.createElement(App, {},
					React.createElement(StaticRouter, { location, context},
						renderRoutes(staticRoutes)
					)
				)
			);
			renderedComponent = renderToString(components);
		} catch(err) {
			return reject(err);
		}

		return resolve({
			renderedComponent
		});
	});
}

const reactHandler = (app, server) => {
	return (cb) => {
		return app.pr((req, res, next) => {
			const store = buildServerStore({
				app: {
					version: 1234
				}
			});

			let result = cb(store, req, res, next);
			if (!result || !result.then) {
				result = Promise.resolve(result);
			}

			return result
			.then(() => renderApplication(store, req))
			.then(({ renderedComponent }) => {
				const initialState = store.getState();

				return new HTMLResponse(indexHtml({ renderedComponent, initialState }));
			})
			.catch((err) => {
				console.log(err);
			});
		});
	}
};

export default (app, server) => {
	server.use((req, res, next) => {
		const parsedUrl = url.parse(req.originalUrl);
		console.log(parsedUrl);
		const branch = matchRoutes(staticRoutes, parsedUrl.pathname);

		if (!branch) {
			return next();
		}

		return reactHandler(app, server)((store) => {
			const promises = branch.map(({ route, match }) => {
				const actions = bindActionCreators(route.actions || {}, store.dispatch);
				return route.loadData && route.loadData({
					actions,
					route,
					match,
					params: req.params,
					query: req.query
				});
			});
			return Promise.all(promises);
		})(req, res);
	});
}