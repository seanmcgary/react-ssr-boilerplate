import * as _ from 'lodash';
import createStore from '../../app/store/index';

function buildStore(data = {}) {
	return _.merge({
		app: {

		}
	}, data);
}

export function buildServerStore(storeData = {}) {
	const mergedData = _.merge(buildStore(), storeData);
	return createStore(mergedData);
}