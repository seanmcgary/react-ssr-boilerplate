
export class BaseError extends Error {
	constructor(err, data = {}, statusCode = 400) {
		super(err.message);

		this.error = err;
		this.data = data;
		this.statusCode = statusCode;
	}

	toJSON() {
		return {
			message: this.error.message,
			data: this.data
		};
	}
}

export class NotFoundError extends BaseError {
	constructor(err, data = {}) {
		super(err, data, 404);
	}
}

export class AccessDeniedError extends BaseError {
	constructor(err, data = {}) {
		super(err, data, 401);
	}
}