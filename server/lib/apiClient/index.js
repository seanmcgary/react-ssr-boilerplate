import Promise from 'bluebird';
import request from 'request-promise';

import config from '../../../config';

export class APIClient {
	constructor({ host, headers = {}, query = {} } = config) {
		this.host = host;
		this.headers = headers;
		this.query = query;
	}

	makeRequest(method = 'GET', { path = '/', headers = {}, queryParams = {}, data = {}}) {
		const options = {
			method,
			uri: `${this.host}${path}`,
			qs: {
				...this.query,
				...queryParams,
			},
			headers: {
				...this.headers,
				...headers
			},
			resolveWithFullResponse: true,
			json: true
		};

		if (method !== 'GET') {
			options.body = data;
		}
		console.log(options);

		return request(options);
	}
}

export default new APIClient(config.api);