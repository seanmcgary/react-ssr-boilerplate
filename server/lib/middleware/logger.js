import * as _ from 'lodash';
import { v4 } from 'uuid';

function generateRequestId(req, res) {
	return v4();
}

export function logger(options = {}) {
	return (req, res, next) => {
		const requestId = typeof options.generateRequestId === 'function' ? options.generateRequestId(req, res) : generateRequestId(req, res);

		_.set(req, 'requestId', requestId);

		const startTime = Date.now();

		const oldEnd = res.end;

		function newEnd(chunk, encoding, cb) {
			const delta = Date.now() - startTime;
			const statusCode = res.statusCode;

			const logData = {
				requestId: requestId,
				responseTime: delta,
				method: req.method,
				statusCode: statusCode,
				url: req.url
			};

			console.log(JSON.stringify(logData));
			oldEnd.apply(res, _.values(arguments));
		}

		// small hack to get around weird TS typing shenanigans
		res.end = newEnd;

		next();
	};
}