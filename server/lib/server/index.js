import * as _ from 'lodash';
import express from 'express';
import Promise from 'bluebird';
import bodyParser from 'body-parser';
import serveStatic from 'serve-static';
import path from 'path';

import * as middleware from '../middleware';
import { BaseResponse, JSONResponse } from '../response';
import { BaseError } from '../errors';


function promisifyRoute(fn, options) {
	return (req, res, next) => {

		return Promise.resolve(fn(req, res, next)).then((data) => {
			if (!(data instanceof BaseResponse)) {
				data = new JSONResponse(data);
			}

			return data;
		})
		.catch((error) => {
			if (!(error instanceof BaseError)) {
				error = new BaseError(error);
			}

			return JSONResponse.fromError(error);
		})
		.then((data) => data.send(res));
	};
}

export default class WebServer {

	constructor(config) {
		this.config = config;
		this.promisifyRoute = promisifyRoute;
		this.pr = promisifyRoute;

		this.server = express();
		this.server.use('/dist', serveStatic(path.resolve(`${__dirname}/../../../app-dist`)));
		this.server.use(bodyParser.json());
		this.server.use(middleware.logger());
		this.server.use((req, res, next) => {
			next();
		});
	}

	initializeErrorHandler() {
		this.server.use((err, req, res, next) => {
			console.log('ERROR: ', err);

		});
	}

	startServer() {
		this.initializeErrorHandler();
		this.server.listen(this.config.port, () => {
			console.log('listening on port', this.config.port);
		});
	}

	loadRoutes(fn, injectedMiddlewares = {}) {
		fn(this, this.server, {
			...middleware,
			...injectedMiddlewares
		});
	}

	parseCursorFromQuery(queryParams = {}) {
		const params = {};

		if ('limit' in queryParams) {
			params.limit = parseInt(_.get(queryParams, 'limit'));
		}

		if ('offset' in queryParams) {
			params.offset = parseInt(_.get(queryParams, 'offset'));
		}

		if ('orderDirection' in queryParams && 'orderField' in queryParams) {
			params.order = {
				direction: _.get(queryParams, 'orderDirection'),
				field: _.get(queryParams, 'orderField')
			}
		}

		return params;
	}
}