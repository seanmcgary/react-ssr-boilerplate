import * as _ from 'lodash';
import { BaseError } from'../errors';

export class BaseResponse {
	static fromError(err) {
		return new BaseResponse(err, err.statusCode);
	}

	constructor(data, statusCode = 200, headers = {}) {
		this.data = data;
		this.statusCode = statusCode;
		this.headers = headers;
	}

	send(res) {
		_.each(this.headers, (v, h) => res.set(h, v));

		this.sendPayload(res);
	}

	sendPayload(res) {
		res.status(this.statusCode).send(this.data);
	}
}

export class HTTPResponse extends BaseResponse {}

export class JSONResponse extends BaseResponse {
	static fromError(err) {
		return new JSONResponse(err, err.statusCode);
	}

	sendPayload(res) {
		res.status(this.statusCode).json(this.data);
	}
}

export class HTMLResponse extends BaseResponse {
	static fromError(err) {
		return new HTMLResponse(err, err.statusCode);
	}

	sendPayload(res) {
		res.status(this.statusCode).send(this.data);
	}
}