import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { renderRoutes } from 'react-router-config';

import createStore from './store';
import App from './components/app';
import Routes, { staticRoutes } from './routes';

const store = createStore(_.get(window, '__PRELOADED_STATE__'));

function Application() {
	return (
		<Provider store={store}>
			<App>
				<BrowserRouter>
					{renderRoutes(staticRoutes)}
				</BrowserRouter>
			</App>
		</Provider>
	);
}

ReactDOM.hydrate(<Application />, document.getElementById('application-container'));