import React from 'react';
import classnames from 'classnames';
import { compose } from 'recompose';
import { connect } from 'react-redux';

export const enhance = compose();

export function Home() {
	return (
		<div className={classnames('home-page', 'community-page')}>
			test
		</div>
	);
}

export default enhance(Home);