import * as React from 'react';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import api from './api';

const middlewares = [
	thunk,
	api,

	// final thing
	logger
];

export default middlewares;