import { omit, get } from 'lodash';
import axios from 'axios';

export const CALL_API = 'CALL_API';

export function callApi(endpoint, options = {
	method: 'GET',
	headers: {},
	query: {},
	data: {}
}) {
	return axios({
		method: options.method,
		url: `${endpoint}`,
		data: options.data,
		params: options.query,
		headers: options.headers,
		validateStatus: (status) => status < 400
	})
	.then((response) => get(response, 'data') || {});
}

export default (store) => (next) => (action = {}) => {
	const callService = action[CALL_API];

	if (typeof callService === 'undefined') {
		return next(action);
	}

	const { types, method, data, query } = callService;
	const [requestType, successType, failureType] = types;
	const state = store.getState();

	let { endpoint } = callService;
	if (typeof endpoint === 'function') {
		endpoint = endpoint(state);
	}

	function actionWith(actionData) {
		return omit({
			...action,
			...actionData
		}, CALL_API);
	}

	// Fire Request Action
	next(actionWith({
		type: requestType,
		data,
		query
	}));

	const options = {
		method: method || 'GET',
		data,
		query
	};

	if (process.env.RENDER_ENV === 'server') {
		console.log('server render');
	}

	endpoint = `${(typeof window !== 'undefined' && get(window, 'location.origin')) || process.env.REACT_SSR_BOILERPLATE_API_HOST}/api${endpoint}`;

	return callApi(endpoint, options)
	.then((payload) => next(actionWith({
		type: successType,
		payload
	})))
	.catch((error) => next(actionWith({
		type: failureType,
		error
	})));
};
