import { handleActions } from 'redux-actions';

import { PAGES } from './actions';

export const initialState = {
	slug: null,
	posts: []
};

export default handleActions({
	[PAGES.HOMEPAGE_SUCCESS]: (state) => {
		return state;
	}
}, initialState);