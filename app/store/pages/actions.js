import { CALL_API } from '../middleware/api';


export const PAGES = {
	HOMEPAGE_REQUEST: 'PAGES/HOMEPAGE_REQUEST',
	HOMEPAGE_SUCCESS: 'PAGES/HOMEPAGE_SUCCESS',
	HOMEPAGE_FAILURE: 'PAGES/HOMEPAGE_FAILURE',
};

export function getHomepage(query = {}) {
	return {
		[CALL_API]: {
			types: [PAGES.HOMEPAGE_REQUEST, PAGES.HOMEPAGE_SUCCESS, PAGES.HOMEPAGE_FAILURE],
			endpoint: '/homepage',
			query
		}
	};
}