import * as React from 'react';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import app from './app/reducers';
import pages from './pages/reducers';

export default combineReducers({
	app,
	pages,

	form: formReducer
});