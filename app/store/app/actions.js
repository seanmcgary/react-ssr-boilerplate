import React from 'react';
import { createAction, Action } from 'redux-actions';

export const APP = {
	SET: 'APP/SET'
};

export const setApp = createAction(APP.SET, (app) => ({ app }));

