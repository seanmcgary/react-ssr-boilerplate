import React from 'react';
import { handleActions, Action } from 'redux-actions';
import { APP } from './actions';

export const initialState = {

};

export default handleActions({
	[APP.SET]: (state, action) => action.payload.app
}, initialState);