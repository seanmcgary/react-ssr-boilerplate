import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { renderRoutes } from 'react-router-config';

import './appWrapper.scss';

export const enhance = compose();

export function AppWrapper({ route }) {
	return (
		<div className={classnames('app-wrapper')}>
			<div className={classnames('app-wrapper-content')}>
				{renderRoutes(route.routes)}
			</div>
		</div>
	);
}

export default enhance(AppWrapper);