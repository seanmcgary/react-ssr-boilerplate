import React from 'react';
import classnames from 'classnames';
import { Field, reduxForm } from 'redux-form';

import Headline from '../lib/headline';
import { FormRow, FormItem } from '../lib/formRow';
import { Input } from '../lib/input';
import { Button } from '../lib/button';

import './signupForm.scss';

export const signupReduxForm = reduxForm({
	form: 'signup-form',
	fields: ['firstName', 'lastName', 'email', 'password']
});


function renderInput({ input, label, type }) {
	return (
		<Input {...input} placeholder={label} type={type} />
	);
}

export function SignupForm({ handleSubmit, onSubmit }) {
	return (
		<form className={classnames('signup-form')} onSubmit={handleSubmit(onSubmit)}>
			<Headline as="h4">Signup</Headline>
			<FormRow>
				<FormItem>
					<Field
						name="firstName"
						type="text"
						label="First name"
						component={renderInput}
					/>
				</FormItem>
				<FormItem>
					<Field
						name="lastName"
						type="text"
						label="Last name"
						component={renderInput}
					/>
				</FormItem>
			</FormRow>
			<FormRow>
				<FormItem>
					<Field
						name="email"
						type="text"
						label="Email address"
						component={renderInput}
					/>
				</FormItem>
			</FormRow>
			<FormRow>
				<FormItem>
					<Field
						name="password"
						type="password"
						label="Password"
						component={renderInput}
					/>
				</FormItem>
			</FormRow>
			<FormRow>
				<FormItem>
					<Button type="submit">
						Signup
					</Button>
				</FormItem>
			</FormRow>
		</form>
	);
}

export default signupReduxForm(SignupForm);