import React from 'react';
import classnames from 'classnames';
import { Field, reduxForm } from 'redux-form';

import Headline from '../lib/headline';
import { FormRow, FormItem } from '../lib/formRow';
import { Input } from '../lib/input';
import { Button } from '../lib/button';

import './loginForm.scss';

export const loginReduxForm = reduxForm({
	form: 'login-form',
	fields: ['email', 'password']
});


function renderInput({ input, label, type }) {
	return (
		<Input {...input} placeholder={label} type={type} />
	);
}

export function LoginForm({ handleSubmit, onSubmit }) {
	return (
		<form className={classnames('login-form')} onSubmit={handleSubmit(onSubmit)}>
			<Headline as="h4">Login</Headline>
			<FormRow>
				<FormItem>
					<Field
						name="email"
						type="text"
						label="Email address"
						component={renderInput}
					/>
				</FormItem>
			</FormRow>
			<FormRow>
				<FormItem>
					<Field
						name="password"
						type="password"
						label="Password"
						component={renderInput}
					/>
				</FormItem>
			</FormRow>
			<FormRow>
				<FormItem>
					<Button type="submit">
						Login
					</Button>
				</FormItem>
			</FormRow>
		</form>
	);
}

export default loginReduxForm(LoginForm);