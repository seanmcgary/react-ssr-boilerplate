import React from 'react';

import './app.scss';

export default function App(props) {
	return props.children;
}