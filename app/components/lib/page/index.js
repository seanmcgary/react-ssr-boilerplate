import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './page.scss';

export function Page(props) {
	return (
		<div className={classnames('page', props.pageName)}>
			{props.children}
		</div>
	);
}

Page.propTypes = {
	pageName: PropTypes.string.isRequired
};

export default Page;