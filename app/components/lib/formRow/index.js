import React from 'react';
import classnames from 'classnames';
import propTypes from 'prop-types';

import './formRow.scss';

export function FormRow(props) {
	return (
		<div className={classnames('form-row')} children={props.children} />
	);
}

export function FormItem(props) {
	return (
		<div className={classnames('form-item')} children={props.children} />
	);
}