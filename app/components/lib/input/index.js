import React from 'react';
import classnames from 'classnames';
import propTypes from 'prop-types';

import './input.scss';

export function Input(props) {
	return (
		<input
			{...props}
			className={classnames(props.className, 'input', props.size)}
		/>
	);
}

Input.propTypes = {
	size: propTypes.string,
	type: propTypes.string
};

Input.defaultProps = {
	size: 'regular',
	type: 'text'
};