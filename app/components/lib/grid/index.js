import React from 'react';
import classnames from 'classnames';
import propTypes from 'prop-types';

import './grid.scss';

export function Grid(props) {
	return (
		<div className={classnames('grid', props.direction)} children={props.children} />
	);
}

Grid.propTypes = {
	direction: propTypes.oneOf(['column', 'row'])
};

Grid.defaultProps = {
	direction: 'column'
};