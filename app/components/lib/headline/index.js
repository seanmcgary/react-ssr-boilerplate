import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { compose, componentFromProp, defaultProps } from 'recompose';

import './headline.scss';

export function Headline(props) {
	const InnerHeadline = defaultProps({ as: props.as })(componentFromProp('as'));

	return <InnerHeadline {...props} className={classnames(props.className, 'headline')}/>
}

Headline.propTypes = {
	as: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6'])
};

Headline.defaultProps = {
	as: 'h1'
};

export default Headline;