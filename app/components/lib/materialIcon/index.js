import React from 'react';
import PropTypes from 'prop-types';

import './materialIcon.scss';

export function MIcon({ icon }) {
	return (
		<i className="material-icons">{icon}</i>
	);
}

MIcon.propTypes = {
	icon: PropTypes.string.isRequired
};

export default MIcon;