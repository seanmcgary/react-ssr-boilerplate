import React from 'react';
import classnames from 'classnames';
import propTypes from 'prop-types';

import './button.scss';

export function Button(props) {
	console.log(props)
	return (
		<button
			{...props}
			className={classnames('button', props.size)}
		/>
	);
}

Button.propTypes = {
	size: propTypes.string,

};

Button.defaultProps = {
	size: 'regular'
};