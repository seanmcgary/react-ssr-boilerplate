import React from 'react';
import { Switch } from 'react-router';
import { Route } from 'react-router-dom';

import Home from '../pages/home';
import AppWrapper from '../components/appWrapper';

import { getHomepage } from '../store/pages/actions';

export const staticRoutes = [
	{
		path: '/',
		component: AppWrapper,
		routes: [
			{
				path: '/',
				exact: true,
				component: Home,
				actions: { getHomepage },
				loadData: (props) => props.actions.getHomepage()
			}
		]
	}
];

export default function Routes() {
	return (
		<Switch>
			{staticRoutes.map((route, i) => (
				<Route {...route} key={i} />
			))}
		</Switch>
	);
}