import { Config } from 'config-composer';

export default (Override) => {
	return new Config({
		host: Override({
			value: 'http://localhost:8000',
			env: 'API_HOST'
		}).string()
	});
};