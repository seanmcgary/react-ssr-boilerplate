import { Config } from 'config-composer';

export default (Override) => {
	return new Config({
		port: Override({
			value: '9000',
			env: 'SERVER_HOST'
		}).int()
	});
};