import Composer from 'config-composer';
import { inspect } from 'util';

import server from './server';
import api from './api';

const cc = new Composer({
	envPrefix: 'REACT_SSR_BOILERPLATE_'
});

cc.setConfig(server, 'server');
cc.setConfig(api, 'api');

const config = cc.getConfig();

if (process.env.SHOW_CONFIG === 'true') {
	console.log(inspect(config, true, 5, true));
}

export default config;