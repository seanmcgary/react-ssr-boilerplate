const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const babelLoader = {
	loader: 'babel-loader',
	options: {
		cacheDirectory: false,
		presets: [
			"react",
			[
				"es2015",
				{
					"modules": false
				}
			],
			"es2016"
		]
	}
};


module.exports = {
	devtool: 'inline-source-map',
	entry: {
		main: './app/main.js'
	},
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'app-dist')
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules|server/,
				use: [ babelLoader ]
			}, {
				test: /\.scss$/,
				use: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
			}
		]
	},
	plugins: [
		new ExtractTextPlugin({ // define where to save the file
			filename: '[name].css',
			allChunks: true,
		})
	],
	resolve: {
		extensions: ['.js', '.scss']
	}
};