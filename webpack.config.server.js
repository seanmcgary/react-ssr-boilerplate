const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const babelLoader = {
	loader: 'babel-loader',
	options: {
		cacheDirectory: false,
		presets: [
			["es2015",  { "modules": false }],
			"stage-0",
			"react",
			"latest"
		],
		retainLines: true
	}
};


module.exports = {
	entry: {
		main: './server/index.js'
	},
	output: {
		filename: 'index.js',
		path: path.resolve(__dirname, 'server-dist')
	},
	target: 'node',
	node: {
		__dirname: true
	},
	devtool: 'inline-source-map',
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				use: [ babelLoader ]
			}, {
				test: /\.scss$/,
				use: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
			}
		]
	},
	plugins: [
		new ExtractTextPlugin({ // define where to save the file
			filename: '[name].css',
			allChunks: true,
		})
	],
	resolve: {
		extensions: ['.js']
	}
};